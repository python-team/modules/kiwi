<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "DTD/xhtml1-strict.dtd">
<html>
  <head>
    <title>API docs for &ldquo;kiwi.tasklet&rdquo;</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
    <link href="apidocs.css" type="text/css" rel="stylesheet" />
    
    
  </head>
  <body>
    <h1 class="module">Module k.tasklet</h1>
    <p>
      <span id="part">Part of <a href="kiwi.html">kiwi</a></span>
      
      
    </p>
    <div>
      
    </div>
    <div>Pseudo-thread (coroutines) framework
<h1 class="heading">Introduction</h1>
  <p>This module adds infrastructure for managing tasklets.  In this 
  context, a tasklet is defined as a routine that explicitly gives back 
  control to the main program a certain points in the code, while waiting 
  for certain events.  Other terms that may be used to describe tasklets 
  include <i>coroutines</i>, or <i>cooperative threads</i>.</p>
  The main advantages of tasklets are:
  <ul>
    <li>
      Eliminates the danger of unexpected race conditions or deadlocks 
      that happen with preemptive (regular) threads;
    </li>
    <li>
      Reduces the number of callbacks in your code, that sometimes are 
      so many that you end up with <i>spaghetti code</i>.
    </li>
  </ul>
  <p>The fundamental block used to create tasklets is Python's generators. 
  Generators are objects that are defined as functions, and when called 
  produce iterators that return values defined by the body of the function,
  specifically <code>yield</code> statements.</p>
  The neat thing about generators are not the iterators themselves but 
  the fact that a function's state is completely frozen and restored 
  between one call to the iterator's <code>next()</code> and the following 
  one. This allows the function to return control to a program's main loop 
  while waiting for an event, such as IO on a socket, thus allowing other 
  code to run in the mean time.  When the specified event occurs, the 
  function regains control and continues executing as if nothing had 
  happened.
<h1 class="heading">Structure of a tasklet</h1>
  At the outset, a tasklet is simply a python <a 
  href="http://www.python.org/peps/pep-0255.html" target="_top">generator 
  function</a>, i.e. a function or method containing one or more 
  <code>yield</code> statements.  Tasklets add a couple more requirements 
  to regular generator functions:
  <ol start="1">
    <li>
      The values contained in <code>yield</code> statements cannot be 
      arbitrary (see below);
    </li>
    <li>
      After each <code>yield</code> that indicates events, the function 
      <a 
      href="kiwi.tasklet.html#get_event"><code>kiwi.tasklet.get_event</code></a>
      must be called to retrieve the event that just occurred.
    </li>
  </ol>
<h1 class="heading">Syntax for yield in tasklets</h1>
  Inside tasklet functions, <code>yield</code> statements are used to 
  suspend execution of the tasklet while waiting for certain events.  Valid
  <code>yield</code> values are:
  <ul>
    <li>
      A single <a 
      href="kiwi.tasklet.Message.html"><code>Message</code></a> object, 
      with a correctly set <i>dest</i> parameter.  With this form, a 
      message is sent to the indicated tasklet.  When <code>yield</code> 
      returns, no event is generated, so the tasklet should <b>not</b> call
      <a href="kiwi.tasklet.html#get_event"><code>get_event</code></a>.
    </li>
    <li>
      One, or a sequence of:
      <ul>
        <li>
          <a 
          href="kiwi.tasklet.WaitCondition.html"><code>WaitCondition</code></a>,
          meaning to wait for that specific condition
        </li>
        <li>
          <a href="kiwi.tasklet.Tasklet.html"><code>Tasklet</code></a>, 
          with the same meaning as <a 
          href="kiwi.tasklet.WaitForTasklet.html"><code>WaitForTasklet</code></a><code>(tasklet)</code>
        </li>
        <li>
          generator, with the same meaning as <a 
          href="kiwi.tasklet.WaitForTasklet.html"><code>WaitForTasklet</code></a><code>(Tasklet(gen))</code>
        </li>
      </ul>
      In this case, the tasklet is suspended until either one of the 
      indicated events occurs.  The tasklet must call <a 
      href="kiwi.tasklet.html#get_event"><code>get_event</code></a> in this
      case.
    </li>
  </ul>
<h1 class="heading">Launching a tasklet</h1>
  To start a tasklet, the <a 
  href="kiwi.tasklet.Tasklet.html"><code>Tasklet</code></a> constructor 
  must be used:
<pre class="literalblock">
 from kiwi import tasklet

 def my_task(x):
     [...]

 tasklet.Tasklet(my_task(x=0))
</pre>
  Alternatively, <a 
  href="kiwi.tasklet.html#run"><code>kiwi.tasklet.run</code></a> can be 
  used to the same effect:
<pre class="literalblock">
 from kiwi import tasklet
 tasklet.run(my_task(x=0))
</pre>
  Yet another approach is to use the @tasklet.task decorator:
<pre class="literalblock">
 from kiwi import tasklet

 @tasklet.task
 def my_task(x):
     [...]
     raise StopIteration(&quot;return value&quot;)

 yield my_task(x=0)
 retval = tasklet.get_event().retval
</pre>
<h1 class="heading">Examples</h1>
  <h2 class="heading">Background timeout task</h2>
    This example demonstrates basic tasklet structure and timeout 
    events:
<pre class="literalblock">
 import gobject
 from kiwi import tasklet

 mainloop = gobject.MainLoop()

 def simple_counter(numbers):
     timeout = tasklet.WaitForTimeout(1000)
     for x in xrange(numbers):
         print x
         yield timeout
         tasklet.get_event()
     mainloop.quit()

 tasklet.run(simple_counter(10))
 mainloop.run()
</pre>
  <h2 class="heading">Message passing</h2>
    This example extends the previous one and demonstrates message 
    passing:
<pre class="literalblock">
 import gobject
 from kiwi import tasklet

 mainloop = gobject.MainLoop()

 @tasklet.task
 def printer():
     msgwait = tasklet.WaitForMessages(accept=(&quot;quit&quot;, &quot;print&quot;))
     while True:
         yield msgwait
         msg = tasklet.get_event()
         if msg.name == &quot;quit&quot;:
             return
         assert msg.name == 'print'
         print &quot;&gt;&gt;&gt; &quot;, msg.value

 @tasklet.task
 def simple_counter(numbers, task):
     timeout = tasklet.WaitForTimeout(1000)
     for x in xrange(numbers):
         yield tasklet.Message('print', dest=task, value=x)
         yield timeout
         tasklet.get_event()
     yield tasklet.Message('quit', dest=task)
     mainloop.quit()

 task = printer()
 simple_counter(10, task)
 mainloop.run()
</pre>
<table class="fieldTable"></table></div>
    
    
    <div id="splitTables">
      <table class="children sortable" id="id382">
  
  
  
  
  <tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.task.html">task</a></td>
    <td><span>A decorator that modifies a tasklet function to avoid the need
</span></td>
  </tr><tr class="function">
    
    
    <td>Function</td>
    <td><a href="kiwi.tasklet.html#get_event">get_event</a></td>
    <td><span>Return the last event that caused the current tasklet to regain 
control.
</span></td>
  </tr><tr class="function">
    
    
    <td>Function</td>
    <td><a href="kiwi.tasklet.html#run">run</a></td>
    <td><span>Start running a generator as a <a 
href="kiwi.tasklet.Tasklet.html"><code>Tasklet</code></a>.
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.WaitCondition.html">WaitCondition</a></td>
    <td><span>Base class for all wait-able condition objects.
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.WaitForCall.html">WaitForCall</a></td>
    <td><span>An object that waits until it is called.
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.WaitForIO.html">WaitForIO</a></td>
    <td><span>An object that waits for IO conditions on sockets or file
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.WaitForTimeout.html">WaitForTimeout</a></td>
    <td><span>An object that waits for a specified ammount of time (a timeout)
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.WaitForIdle.html">WaitForIdle</a></td>
    <td><span>An object that waits for the main loop to become idle
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.WaitForTasklet.html">WaitForTasklet</a></td>
    <td><span>An object that waits for a tasklet to complete
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.WaitForSignal.html">WaitForSignal</a></td>
    <td><span>An object that waits for a signal emission
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.WaitForProcess.html">WaitForProcess</a></td>
    <td><span>An object that waits for a process to end
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.Message.html">Message</a></td>
    <td><span>A message that can be received by or sent to a tasklet.
</span></td>
  </tr><tr class="function">
    
    
    <td>Function</td>
    <td><a href="kiwi.tasklet.html#_normalize_list_argument">_normalize_list_argument</a></td>
    <td><span>returns a list of strings from an argument that can be either
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.WaitForMessages.html">WaitForMessages</a></td>
    <td><span>An object that waits for messages to arrive
</span></td>
  </tr><tr class="class">
    
    
    <td>Class</td>
    <td><a href="kiwi.tasklet.Tasklet.html">Tasklet</a></td>
    <td><span>An object that launches and manages a tasklet.
</span></td>
  </tr>
  
</table>
      
      
    </div>
    
    
    
    <div class="function">
        <a name="kiwi.tasklet.get_event"></a>
        <a name="get_event"></a>
        <div class="functionHeader">
        
        def get_event():
        
      </div>
        <div class="functionBody">
          
          <div>Return the last event that caused the current tasklet to regain 
control.
<table class="fieldTable"><tr class="fieldStart"><td class="fieldName">Note</td><td colspan="2">this function should be called exactly once after each yield that 
includes a wait condition.
</td></tr></table></div>
        </div>
      </div><div class="function">
        <a name="kiwi.tasklet.run"></a>
        <a name="run"></a>
        <div class="functionHeader">
        
        def run(gen):
        
      </div>
        <div class="functionBody">
          
          <div>Start running a generator as a <a 
href="kiwi.tasklet.Tasklet.html"><code>Tasklet</code></a>.
<table class="fieldTable"><tr class="fieldStart"><td class="fieldName">Parameters</td><td class="fieldArg">gen</td><td>generator object that implements the tasklet body.
</td></tr><tr class="fieldStart"><td class="fieldName">Returns</td><td colspan="2">a new <a href="kiwi.tasklet.Tasklet.html"><code>Tasklet</code></a> 
instance, already running.
</td></tr><tr class="fieldStart"><td class="fieldName">Note</td><td colspan="2">this is strictly equivalent to calling <code>Tasklet(gen)</code>.
</td></tr></table></div>
        </div>
      </div><div class="function">
        <a name="kiwi.tasklet._normalize_list_argument"></a>
        <a name="_normalize_list_argument"></a>
        <div class="functionHeader">
        
        def _normalize_list_argument(arg, name):
        
      </div>
        <div class="functionBody">
          
          <div>returns a list of strings from an argument that can be either list of 
strings, None (returns []), or a single string returns ([arg])
<table class="fieldTable"></table></div>
        </div>
      </div>
    
    <address>
      <a href="index.html">API Documentation</a> for Kiwi, generated by <a href="http://codespeak.net/~mwh/pydoctor/">pydoctor</a> at 2008-05-29 16:06:17.
    </address>
  </body>
</html>